-module(mod_sass).
-author("Steffen Hanikel").

-mod_title("Sass Compiler").
-mod_description("Sass - Syntactically Awesome Stylesheets.").
-mod_prio(900).
-mod_depends([]).
-mod_provides([sass]).

