-module(controller_sass).
-export([init/1,
  allowed_methods/2,
  resource_exists/2,
  content_types_provided/2,
  charsets_provided/2,
  content_encodings_provided/2,
  provide_content/2,
  is_authorized/2,
  get_checksum/2, last_modified/2, calc_checksum/1, expires/2, process/1]).

-include_lib("controller_webmachine_helper.hrl").
-include_lib("zotonic.hrl").

-define(MAX_AGE, 315360000).

-record(state, {
  path,
  fullpaths,
  name,
  args,
  context,
  data,
  last_modified,
  is_cached=false,
  checksum
}).

-record(cache, {
  path=undefined,
  fullpaths=undefined,
  mime=undefined,
  last_modified=undefined,
  data=undefined,
  checksum
}).

-record(cache_paths, {
  fullpaths=undefined
}).


init(DispatchArgs) -> {ok, #state{args=DispatchArgs}}.

allowed_methods(ReqData, State) ->
  {['HEAD', 'GET'], ReqData, State}.

% We don't allow parsing of every file as scss, because of possible security issues.
is_authorized(ReqData, State) ->
  State1 = get_name(ReqData,State),
  case lists:suffix(".scss", State1#state.name) of
    true ->
      {true, ReqData, State1};
    false ->
      {{halt, 403}, ReqData, State1}
  end.

content_encodings_provided(ReqData, State) ->
  {["identity", "gzip"], ReqData, State}.

charsets_provided(ReqData, State) ->
  {["utf-8"], ReqData, State}.

content_types_provided(ReqData, State) ->
  {[{"text/css", provide_content}], ReqData, State}.

expires(ReqData, State) ->
  NowSecs = calendar:datetime_to_gregorian_seconds(calendar:universal_time()),
  {calendar:gregorian_seconds_to_datetime(NowSecs + ?MAX_AGE), ReqData, State}.

resource_exists(ReqData, State) ->
  State1 = init_context(ReqData, State),
  State2 = get_name(ReqData, State1),
  State3 = lookup_path(State2),
  case State3#state.path of
    {error, enoent} ->
      {false, ReqData, State3};
    _ ->
      {true, ReqData, State3}
  end.

last_modified(ReqData, State) ->
  RD1 = wrq:set_resp_header("Cache-Control", "public, max-age="++integer_to_list(?MAX_AGE), ReqData),
  State1 = get_cache(State),
  {State1#state.last_modified, RD1, State1}.

provide_content(ReqData, State) ->
  State1 = get_cache(State),
  {select_encoding(State1#state.data, ReqData), ReqData, State1}.

cache_key(Name) ->
  {?MODULE, Name}.

get_cache(#state{name=Name, checksum=Checksum, context=Context, data=undefined} = State) ->
  case z_depcache:get(cache_key({Name, Checksum}), Context) of
    undefined ->
      process(State);
    {ok, Cache} ->
      State#state{
        is_cached=true,
        path=Cache#cache.path,
        fullpaths=Cache#cache.fullpaths,
        last_modified=Cache#cache.last_modified,
        data=Cache#cache.data,
        checksum=Cache#cache.checksum
      }
  end;
get_cache(State) ->
  State.

% Need Path
process(#state{path=Path} = State) ->
  Sass = z_config:get(sassc_path,"/usr/bin/sassc"),
  case exec:run([Sass, "-p", "-t", "compressed", Path], [stdout, stderr, sync]) of
    {ok, Out} ->
      Data=encode_data(proplists:get_value(stdout, Out, "")),
      FullPaths=split_paths(proplists:get_value(stderr, Out, "")),
      save_cache(calc_checksum(State#state{
        data=Data,
        fullpaths=case FullPaths of
                    [] ->
                      [Path];
                    P ->
                      P
                  end
      }));
    {error, Errors} ->
      State#state{data=encode_data(proplists:get_value(stderr, Errors, Errors)), fullpaths=[Path]}
  end.

calc_checksum(#state{fullpaths=FullPaths} = State) ->
  Ts = lists:filter(fun (T) -> T =/= 0 end, lists:map(fun filelib:last_modified/1, FullPaths)),
  [LastModified|_] = calendar:local_time_to_universal_time_dst(lists:max([{{1970, 1, 1}, {0, 0, 0}}|Ts])),
  Checksum = lists:sum(lists:map(fun calendar:datetime_to_gregorian_seconds/1, Ts)),
  State#state{
    last_modified=LastModified,
    checksum=Checksum
  }.



% Cache the served file in the depcache.  Cache it for 3600 secs.
save_cache(State) ->
  Cache = #cache{
    path=State#state.path,
    fullpaths=State#state.fullpaths,
    last_modified=State#state.last_modified,
    data=State#state.data,
    checksum=State#state.checksum
  },
  CacheP = #cache_paths{
    fullpaths=State#state.fullpaths
  },
  z_depcache:set(cache_key({State#state.name, State#state.checksum}), Cache, 3600, [module_index], State#state.context),
  z_depcache:set(cache_key(State#state.name), CacheP, 3600, [module_index], State#state.context),
  State.

split_paths(Input) ->
  lists:filter(fun (S) -> S =/= [] end, lists:map(fun erlang:binary_to_list/1, lists:append(lists:map(fun (In) -> binary:split(In, <<"\n">>, [global]) end, Input)))).

init_context(ReqData, #state{context=undefined} = State) ->
  State#state{context=z_context:new(ReqData, ?MODULE)};
init_context(_ReqData, State) ->
  State.

lookup_path(#state{path=undefined, context=Context, name=Name} = State) ->
  case z_module_indexer:find(lib, Name, Context) of
    {ok, #module_index{filepath=Path}} ->
      State#state{path=Path};
    {error, enoent} ->
      State#state{path={error, enoent}}
  end;
lookup_path(State) ->
  State.

get_name(ReqData, #state{name=undefined} = State) ->
  {Name, Checksum} = scomp_sass_sass:split_link(mochiweb_util:unquote(wrq:disp_path(ReqData))),
  State#state{name=Name, checksum=Checksum};
get_name(_ReqData, State) ->
  State.

select_encoding(Data, ReqData) ->
  decode_data(wrq:resp_content_encoding(ReqData), Data).

%% Encode the data so that the identity variant comes first and then the gzip'ed variant
encode_data(Data) ->
  {Data, zlib:gzip(Data)}.

decode_data("identity", {Data, _Gzip}) ->
  Data;
decode_data("identity", Data) ->
  Data;
decode_data("gzip", {_Data, Gzip}) ->
  Gzip;
decode_data("gzip", Data) ->
  zlib:gzip(Data).

get_cache_paths(#state{name=Name, context=Context, fullpaths=undefined} = State) ->
  case z_depcache:get(cache_key(Name), Context) of
    undefined ->
      process(State);
    {ok, Cache} ->
      State#state{
        fullpaths=Cache#cache_paths.fullpaths
      }
  end.

get_checksum(Name, Context) ->
  State = lookup_path(#state{context=Context, name=Name}),
  case (State#state.path) of
    {error, enoent} ->
      0;
    _ ->
      % First get the cached paths then recalc the checksum, we don't need to save that
      State1 = calc_checksum(get_cache_paths(State)),
      State1#state.checksum
  end.