-module(scomp_sass_sass).
-behaviour(gen_scomp).

-export([vary/2, render/3, split_link/1]).

-include("zotonic.hrl").

-define(SEP, $~).

vary(_Params, _Context) -> nocache.

render(Params, _Vars, Context) ->
  {ok, lists:map(fun(Param) -> get_link(Param, Context) end, Params)}.

get_link({_, Id}, Context) ->
  ["<link rel=\"stylesheet\" type=\"text/css\" href=\"/sass/", Id, ?SEP, integer_to_list(controller_sass:get_checksum(Id, Context)), "\">"].

split_link([]) ->
  {"", undefined};
split_link(Link) ->
  case string:tokens(Link,[?SEP]) of
    [Path | [Checksum]] ->
      {Path, list_to_integer(Checksum)};
    [Path | _] ->
      {Path, undefined}
  end.